<?php

namespace LCB\Core\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\LocalizedException;

class Merge extends Command
{
    protected $_configInterface;
   
    public function __construct(    
        \Magento\Framework\App\Config\ConfigResource\ConfigInterface $configInterface
    ) {
        $this->_configInterface = $configInterface;
      
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('lcb:merge')->setDescription('Merge, unmerge static resources under production mode.');
        $this->addArgument('resource', InputArgument::OPTIONAL, __('Type a resource (css/js)'));
        $this->addArgument('action', InputArgument::OPTIONAL, __('Type an action: 0 for unmerge, 1 for merge, 2 for merge and minify'), 1);
    }

    /**
     * Merge or unmerge static files
     * 
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $resource = $input->getArgument('resource');
        $action = (int) $input->getArgument('action');

        switch ($resource) {
            case 'css':
                if ($action === 0) {
                    $this->_configInterface->saveConfig('dev/css/merge_css_files', 0, 'default', 0);
                    $this->_configInterface->saveConfig('dev/css/minify_files', 0, 'default', 0);
                } elseif ($action == 1) {
                    $this->_configInterface->saveConfig('dev/css/merge_css_files', 1, 'default', 0);
                    $this->_configInterface->saveConfig('dev/css/minify_files', 0, 'default', 0);
                } elseif ($action === 2) {
                    $this->_configInterface->saveConfig('dev/css/merge_css_files', 1, 'default', 0);
                    $this->_configInterface->saveConfig('dev/css/minify_files', 1, 'default', 0);
                }
                break;
            case 'js':
                if ($action === 0) {
                    $this->_configInterface->saveConfig('dev/js/merge_files', 0, 'default', 0);
                    $this->_configInterface->saveConfig('dev/js/minify_files', 0, 'default', 0);
                } elseif ($action == 1) {
                    $this->_configInterface->saveConfig('dev/js/merge_files', 1, 'default', 0);
                    $this->_configInterface->saveConfig('dev/js/minify_files', 0, 'default', 0);
                } elseif ($action === 2) {
                    $this->_configInterface->saveConfig('dev/js/merge_files', 1, 'default', 0);
                    $this->_configInterface->saveConfig('dev/js/minify_files', 1, 'default', 0);
                }
                break;
            default:
                break;
        }
    }

}